interface errors {
    [error: string]: string
}

export {
    errors
};
