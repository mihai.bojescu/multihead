const errors = {
    invalid: {
        credentials: "Invalid credentials",
        type: "Invalid type",
        task: "Invalid task",
        command: "Invalid command"
    },
    format: {
        capacities: "Capacities format"
    }
};

export default {
    errors
};
