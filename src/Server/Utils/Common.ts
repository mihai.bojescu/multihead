import { IServer } from "../Types";

function findClient (array: IServer.connection[], client: IServer.Net): number {
    return array.findIndex(item => item.client === client);
}

export default {
    findClient
};
