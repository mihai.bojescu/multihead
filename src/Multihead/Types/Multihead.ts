import net from "net";

import Server, { IServer } from "../../Server";
import Client, { IClient } from "../../Client";

interface EnabledServer extends IServer.config {
    enabled: boolean
    port: number,
    secret: string
}

interface EnabledClient extends IClient.config {
    enabled: boolean
    connectTo: Array<net.AddressInfo>,
    secret: string
}

interface init {
    server?: EnabledServer,
    client?: EnabledClient
}

interface config extends init {
    server: EnabledServer,
    client: EnabledClient
}

type server = Server;
type client = Client;

export {
    init,
    config,
    server,
    client
};
