import Client from "../../Client";
import Server from "../../Server";
import { IMultihead } from "../Types";

class Multihead {
    config: IMultihead.config = {
        server: {
            enabled: false,
            port: 15400,
            secret: ""
        },
        client: {
            enabled: false,
            connectTo: [],
            secret: ""
        }
    };

    server: IMultihead.server | null;
    client: IMultihead.client | null;

    constructor (config?: IMultihead.init) {
        const hasServer = config?.server;
        const hasServerEnabled = config?.server?.enabled;
        const hasServerPort = hasServerEnabled && config?.server?.port;
        const hasServerSecret = hasServerEnabled && config?.server?.secret;

        const hasClient = config?.client;
        const hasClientEnabled = config?.client?.enabled;
        const hasClientConnectedTo = hasClientEnabled && config?.client?.connectTo;
        const hasClientSecret = hasClientEnabled && config?.client?.secret;

        if (hasServer) {
            if (hasServerEnabled) {
                this.config.server.enabled = config?.server?.enabled || this.config.server.enabled;
            }

            if (hasServerPort) {
                this.config.server.port = config?.server?.port || this.config.server.port;
            }

            if (hasServerSecret) {
                this.config.server.secret = config?.server?.secret || this.config.server.secret;
            }
        }

        if (hasClient) {
            if (hasClientEnabled) {
                this.config.client.enabled = config?.client?.enabled || this.config.client.enabled;
            }

            if (hasClientConnectedTo) {
                this.config.client.connectTo = config?.client?.connectTo || this.config.client.connectTo;
            }

            if (hasClientSecret) {
                this.config.client.secret = config?.client?.secret || this.config.client.secret;
            }
        }

        this.server = null;
        this.client = null;

        this.startServer();
        this.startClient();
    }

    getServer (): IMultihead.server | null {
        return this.server;
    }

    getClient (): IMultihead.client | null {
        return this.client;
    }

    private startServer (): void {
        const requestsServer = this.config.server.enabled === true;

        if (requestsServer) {
            const config = {
                port: this.config.server.port,
                secret: this.config.server.secret
            };

            this.server = new Server(config);
        }
    }

    private startClient (): void {
        const requestsClient = this.config.client.enabled === true;

        if (requestsClient) {
            const config = {
                connectTo: this.config.client.connectTo,
                secret: this.config.client.secret
            };

            this.client = new Client(config);
        }
    }
}

export default Multihead;
