const errors = {
    connection: {
        ended: "Disconnected"
    },
    invalid: {
        credentials: "Invalid credentials",
        type: "Invalid type",
        task: "Invalid task",
        init: "Invalid init"
    },
    format: {
        task: "Task format"
    }
};

export default {
    errors
};
