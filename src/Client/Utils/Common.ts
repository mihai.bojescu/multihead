import { IClient } from "../Types";

function findClient (array: IClient.connection[], client: IClient.Net): number {
    return array.findIndex(item => item.client === client);
}

export default {
    findClient
};
