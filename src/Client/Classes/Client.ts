import { IClient } from "../Types";
import { Common, Constants } from "../Utils";
import { Net, Constants as NetConstants } from "../../Common";

class Client {
    config: IClient.config;
    capacities: IClient.capacitiesMap;
    connections: IClient.connection[];
    tasks: IClient.taskMap;

    constructor (config: IClient.config) {
        this.config = config;
        this.capacities = {};
        this.connections = [];
        this.tasks = {
            counter: 0
        };

        this.startClients();
    }

    public addCapacity (capacity: string, callback: IClient.capacityCallback): void {
        this.capacities[capacity] = callback;

        this.connections.forEach(connection => {
            const serverMessage: IClient.serverMessage = {
                type: NetConstants.types.capacities,
                err: "",
                data: {
                    capacities: Object.keys(this.capacities)
                }
            };

            connection.client.send(serverMessage, NetConstants.defaults.length, err => {
                if (err) {
                    this.onError(connection.client)(err, NetConstants.types.capacities, null);
                }
            });
        });
    }

    private startClients () {
        this.config.connectTo.forEach(connection => {
            const client = new Net(connection, null);

            client.receiver.on(NetConstants.events.connect, this.onConnect(client));
            client.receiver.on(NetConstants.events.message, this.onMessage(client));
            client.receiver.on(NetConstants.events.error, this.onError(client));
            client.receiver.on(NetConstants.events.disconnect, this.onDisconnect(client));
        });
    }

    private sendInit (client: IClient.Net) {
        const messageForServer = {
            type: NetConstants.types.init,
            err: "",
            data: {
                secret: this.config.secret
            }
        };

        client.send(messageForServer, NetConstants.defaults.length, err => {
            if (err) {
                this.onError(client)(err, null, null);
            }
        });
    }

    private sendCapacities (client: IClient.Net) {
        const messageForServer = {
            type: NetConstants.types.capacities,
            err: "",
            data: {
                capacities: Object.keys(this.capacities)
            }
        };

        client.send(messageForServer, NetConstants.defaults.length, err => {
            if (err) {
                this.onError(client)(err, null, null);
            }
        });
    }

    private taskCallback (taskId: number, foreignTaskId: number) {
        return (error: Error | null, data: Record<string, unknown>): void => {
            const task = this.tasks[taskId];

            delete this.tasks[taskId];

            if (error) {
                this.onError(task.connection.client)(error, NetConstants.types.task, null);
            } else {
                doAct(this, task, data);
            }
        };

        function doAct (self: Client, task: IClient.task, data: Record<string, unknown>): void {
            const serverMessage: IClient.serverMessage = {
                type: NetConstants.types.task,
                err: "",
                data: {
                    taskId: foreignTaskId,
                    payload: data
                }
            };

            task.connection.client.send(serverMessage, NetConstants.defaults.length, err => {
                if (err) {
                    self.onError(task.connection.client)(err, null, null);
                }
            });
        }
    }

    private onConnect (client: IClient.Net) {
        return () => {
            this.sendInit(client);
        };
    }

    private onMessage (client: IClient.Net) {
        return (message: IClient.serverMessage) => {
            switch (message.type) {
                case NetConstants.types.init:
                    return handleInit(this, client, message);
                case NetConstants.types.capacities:
                    return handleCapacities(this, client, message);
                case NetConstants.types.task:
                    return handleTask(this, client, message);
                default:
                    return this.onError(client)(new Error(Constants.errors.invalid.type), null, null);
            }
        };

        function handleInit (self: Client, client: IClient.Net, message: IClient.serverMessage) {
            const error = message.err;
            const data = message.data;
            const isMessageOk = data?.message === "ok";

            if (error) {
                self.onError(client)(new Error(error), null, null);
            } else if (!isMessageOk) {
                self.onError(client)(new Error(Constants.errors.invalid.init), null, null);
            } else {
                self.sendCapacities(client);
            }
        }

        function handleCapacities (self: Client, client: IClient.Net, message: IClient.serverMessage) {
            const error = message.err;
            const data = message.data;
            const isMessageOk = data?.message === "ok";

            if (error) {
                self.onError(client)(new Error(error), null, null);
            } else if (!isMessageOk) {
                self.onError(client)(new Error(Constants.errors.invalid.init), null, null);
            } else {
                add();
            }

            function add () {
                self.connections.push({
                    client,
                    tasks: []
                });
            }
        }

        function handleTask (self: Client, client: IClient.Net, message: IClient.serverMessage) {
            const messageTaskId = message.data?.taskId;
            const messageCapacity = message.data?.command;
            const messageData = message.data?.payload;
            const messageHasTask = messageTaskId !== undefined && typeof messageTaskId === "number";
            const messageHasCapacity = messageCapacity !== undefined && typeof messageCapacity === "string";
            const messageHasData = messageData !== undefined && typeof messageData === "object";
            const capacity = messageHasCapacity && messageHasData && self.capacities[message.data?.command as string];
            const capacityIsKnown = capacity && typeof capacity === "function";
            const clientIndex = Common.findClient(self.connections, client);
            const clientExists = clientIndex !== -1;

            if (!clientExists) {
                self.onError(client)(new Error(Constants.errors.invalid.credentials), null, null);
            } else if (!messageHasTask || !messageHasCapacity || !messageHasData) {
                self.onError(client)(new Error(Constants.errors.format.task), NetConstants.types.task, null);
            } else if (!capacityIsKnown) {
                self.onError(client)(new Error(Constants.errors.invalid.task), NetConstants.types.task, null);
            } else {
                doAct();
            }

            function doAct () {
                const counter = ++self.tasks.counter;
                const taskCallback = self.taskCallback(counter, messageTaskId as number);
                const connection = self.connections[clientIndex];

                self.tasks[counter] = {
                    taskId: messageTaskId as number,
                    connection: connection
                };

                (capacity as IClient.capacityCallback)(messageData as Record<string, unknown>, taskCallback);
            }
        }
    }

    private onError (client: IClient.Net) {
        return (error: Error, type: string | null, data: Record<string, unknown> | null) => {
            const clientIndex = Common.findClient(this.connections, client);
            const clientExists = clientIndex !== -1;

            if (clientExists) {
                if (!type) {
                    client.close();
                } else {
                    doAct(this, error, type, data, clientIndex);
                }
            }
        };

        function doAct (self: Client, error: Error, type: string | null, data: Record<string, unknown> | null, clientIndex: number) {
            self.connections.splice(clientIndex, 1);

            const serverMessage: IClient.serverMessage = {
                type: type ?? NetConstants.types.error,
                err: error.message,
                data: data ?? {}
            };

            client.send(serverMessage, NetConstants.defaults.length, err => {
                if (err) {
                    client.close();
                }
            });
        }
    }

    private onDisconnect (client: IClient.Net) {
        return () => {
            const clientIndex = Common.findClient(this.connections, client);
            const clientExists = clientIndex !== -1;

            if (clientExists) {
                this.connections[clientIndex].tasks = [];
                this.connections.splice(clientIndex, 1);
            }

            client.close();
        };
    }
}

export default Client;
