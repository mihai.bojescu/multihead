import net from "net";

import { Net as networkDecorator } from "../../Common";

type Net = networkDecorator;

interface config {
    connectTo: Array<net.AddressInfo>,
    secret: string
}

interface capacitiesMap {
    [key: string]: capacityCallback
}

type connection = {
    client: Net,
    tasks: number[]
}

interface taskMap {
    counter: number,
    [task: number]: task
}

interface task {
    taskId: number
    connection: connection
}

type capacityCallback = (data: Record<string, unknown> | void, callback: (err: Error | null, data: Record<string, unknown>) => void) => void

interface serverMessage extends Record<string, unknown> {
    type: string,
    err: string,
    data: Record<string, unknown>
}

export {
    Net,
    config,
    capacitiesMap,
    connection,
    taskMap,
    task,
    capacityCallback,
    serverMessage
};
