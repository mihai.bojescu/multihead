import net from "net";
import events from "events";

import { Constants, Utils } from "../Utils";
import { INet } from "../Types";

class Net {
    connection: net.AddressInfo | null;
    socket: net.Socket | null;
    receiver: events.EventEmitter;
    connected: boolean;
    connectionRetries: number;
    connectionInterval: number;
    maxConnectionRetries: number;
    lastSearchIndex: number;
    message: string;

    constructor (connection: net.AddressInfo | null, socket: net.Socket | null) {
        this.connection = connection || null;
        this.socket = socket || null;
        this.receiver = new events.EventEmitter();
        this.connected = socket !== null && !socket.destroyed;
        this.connectionRetries = 20;
        this.connectionInterval = 500;
        this.maxConnectionRetries = 20;
        this.lastSearchIndex = 0;
        this.message = "";

        this.startSocket();
    }

    private startSocket () {
        if (!this.socket && this.connection) {
            const socketOptions = Utils.toSocketOptions(this.connection);

            this.socket = new net.Socket();
            this.socket.connect(socketOptions);
        }

        this.init();
    }

    private reconnect () {
        setTimeout(() => {
            const socketOptions = Utils.toSocketOptions(this.connection as net.AddressInfo);

            (this.socket as net.Socket).connect(socketOptions);

            if (this.connected) {
                this.connectionRetries--;
            }
        }, this.connectionInterval);
    }

    private init () {
        (this.socket as net.Socket).on("data", onData(this));
        (this.socket as net.Socket).on("error", onError(this));
        (this.socket as net.Socket).on("connect", onConnect(this));
        (this.socket as net.Socket).on("end", onDisconnect(this));

        function onData (self: Net) {
            return (data: Buffer) => {
                if (self.message.length + data.length > 50000000) {
                    return onError(self)(new Error(Constants.errors.invalid.length));
                }

                let indexOfDelimiter = -1;
                const dataString = data.toString();

                self.message += dataString;

                while ((indexOfDelimiter = self.message.indexOf(Constants.defaults.delimiter, self.lastSearchIndex)) !== -1) {
                    parse(self)(self.message.substr(0, indexOfDelimiter));
                    self.message = self.message.substr(indexOfDelimiter + 2, self.message.length - indexOfDelimiter - 2);
                }

                self.lastSearchIndex = self.message.length;
            };
        }

        function parse (self: Net) {
            return (message: string) => {
                let obj = null;

                try {
                    obj = JSON.parse(message);
                } catch {
                    return onError(self)(new Error(Constants.errors.format.parsing));
                }

                onMessage(self)(obj);
            };
        }

        function onMessage (self: Net) {
            return (obj: Record<string, unknown>) => {
                self.receiver.emit(Constants.events.message, obj);
            };
        }

        function onError (self: Net) {
            return (err: Error) => {
                if (self.connection && self.connectionRetries > 0) {
                    self.reconnect();
                } else {
                    (self.socket as net.Socket).end();
                    self.message = "";
                    self.receiver.emit(Constants.events.error, err);
                }
            };
        }

        function onConnect (self: Net) {
            return () => {
                self.connectionRetries = self.maxConnectionRetries;
                self.connected = true;
                self.receiver.emit(Constants.events.connect);
            };
        }

        function onDisconnect (self: Net) {
            return () => {
                if (self.connection && self.connectionRetries > 0) {
                    self.reconnect();
                } else {
                    self.message = "";
                    self.receiver.emit(Constants.events.disconnect);
                }
            };
        }
    }

    send (data: Record<string, unknown>, chunkSize = Constants.defaults.length, callback: INet.sendCallback): void {
        let message = "";

        if (!(this.socket as net.Socket).destroyed) {
            message = JSON.stringify(data) + Constants.defaults.delimiter;

            abstractWrite(this);
        }

        function abstractWrite (self: Net) {
            if (message.length === 0) {
                callback(null);
            } else {
                (self.socket as net.Socket).write(message.substr(0, chunkSize), err => {
                    if (err) {
                        callback(err);
                    } else {
                        message = message.substr(chunkSize, message.length - chunkSize);

                        abstractWrite(self);
                    }
                });
            }
        }
    }

    close (): void {
        (this.socket as net.Socket).end();
        this.message = "";
    }
}

export default Net;
