const errors = {
    invalid: {
        length: "Invalid message length (length overflow)"
    },
    format: {
        parsing: "Parsing"
    },
    client: {
        disconnected: "Client disconnected"
    }
};

const events = {
    message: "message",
    error: "error",
    connect: "connect",
    disconnect: "disconnect"
};

const types = {
    init: "init",
    capacities: "capacities",
    message: "message",
    task: "task",
    ping: "ping",
    pong: "pong",
    error: "error"
};

const defaults = {
    delimiter: "\r\n",
    length: 4096
};

export default {
    errors,
    events,
    types,
    defaults
};
