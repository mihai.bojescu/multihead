import net from "net";

function toSocketOptions (connection: net.AddressInfo): net.TcpNetConnectOpts {
    const socketOptions: net.TcpSocketConnectOpts = {
        port: connection.port,
        host: connection.address,
        family: connection.family
            ? (connection.family === "ipv4" ? 4 : 6)
            : 4
    };

    return socketOptions;
}

export default {
    toSocketOptions
};
