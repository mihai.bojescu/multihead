type sendCallback = (err: Error | null) => void

export {
    sendCallback
};
